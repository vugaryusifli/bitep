var tpj=jQuery; // MAKE JQUERY PLUGIN CONFLICTFREE

tpj.noConflict();
tpj(document).ready(function() {

    if (tpj.fn.cssOriginal != undefined)   // CHECK IF fn.css already extended
        tpj.fn.css = tpj.fn.cssOriginal;

    tpj('.fullwidthbanner').revolution({
        delay:9000,
        startheight:490,
        startwidth:960,

        hideThumbs:200,

        thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Type set to thumb !)
        thumbHeight:50,
        thumbAmount:5,

        navigationType:"bullet",                // bullet, thumb, none
        navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none
        navigationStyle:"round-old",            // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
        touchenabled:"on",                      // Enable Swipe Function : on/off
        onHoverStop:"off",                      // Stop Banner Timer at Hover on Slide on/off

        shadow:0,                               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)
        fullWidth:"on"
    });

});