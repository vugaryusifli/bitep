jQuery(document).ready(function($){

    $('ul.nav .dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(0);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(0);
    });

    $('.nav-tabs li').click(function(){
        $('li').removeClass("active");
        $(this).addClass("active");
    });

});