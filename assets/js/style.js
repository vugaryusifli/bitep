jQuery(document).ready(function($){

    $('ul.nav .dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(0);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(0);
    });


    $('.bxslider').bxSlider({
        speed: 1500,
        pager: false,
        slideMargin: 10,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 3,
        slideWidth: 360
    });

    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: false,
        animationLoop: true,
        itemWidth: 1,
        itemMargin: 1,
        minItems: 2,
        maxItems: 9,
        move: 2
    });

    $('#subscribeForm').submit(function(e) {

        var response = jQuery.ajax({
            type: "post",
            url: 'pages/form_validation',
            data: jQuery(this).serialize(),
            async: false
        }).responseText;
        if(response) {
            $('#subscribeForm').html("<h3>You have successfully subscribed!<h3>");
        }
        else {
            $('#subscribeForm').prepend("<h3>Error! Please try again...<h3>");
        }
        e.preventDefault();

    });

    $("#subscribeForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        }
    });

    var formS = $('#subscribeForm');
    $("#subscr_email").keyup(function(event) {
        if (formS.valid()) {
            $('#subscribe').removeAttr('disabled');
        }else{
            $('#subscribe').attr('disabled', 'disabled');
        }
        event.preventDefault();
    });

});