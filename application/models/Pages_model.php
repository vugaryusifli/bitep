<?php

class Pages_model extends CI_Model{

    public function __construct(){
        parent:: __construct();
    }

    public function get_language(){
        $this->db->select('*');
        $this->db->from('tbl_language');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_menu(){
        $this->db->select('*');
        $this->db->from('tbl_menu');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_slide(){
        $this->db->select('*');
        $this->db->from('tbl_slide');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_page(){
        $this->db->select('*');
        $this->db->from('tbl_pages');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_pricing(){
        $this->db->select('*');
        $this->db->from('tbl_pricing');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_domain(){
        $this->db->select('*');
        $this->db->from('tbl_domain');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_bx_slide(){
        $this->db->select('*');
        $this->db->from('tbl_bxslide');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_flex_slide(){
        $this->db->select('*');
        $this->db->from('tbl_flexslide');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_footer(){
        $this->db->select('*');
        $this->db->from('tbl_footer');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_copyright(){
        $this->db->select('*');
        $this->db->from('tbl_copyright');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_services(){
        $this->db->select('*');
        $this->db->from('tbl_services');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_portfolio(){
        $this->db->select('*');
        $this->db->from('tbl_portfolio');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_contact(){
        $this->db->select('*');
        $this->db->from('tbl_contact');
        $query=$this->db->get();
        return $query->result();
    }

}