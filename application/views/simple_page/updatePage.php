<ul class="nav nav-tabs">
    <li role="presentation" class="active">
        <a href="#">Home</a>
    </li>
    <li role="presentation" class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            About Us <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="#">About Us</a>
            </li>
            <li>
                <a href="#">Manifest</a>
            </li>
        </ul>
    </li>
    <li role="presentation">
        <a href="#">Services</a>
    </li>
    <li role="presentation">
        <a href="#">Portfolio</a>
    </li>
    <li role="presentation">
        <a href="#">Contact Us</a>
    </li>
    <li role="presentation">
        <a href="#">Blog</a>
    </li>
</ul>