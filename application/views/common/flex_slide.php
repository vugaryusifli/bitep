
<!--=== Flex-Slider ===-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="headline">
                <?php foreach($flexslide as $fl): ?>
                    <h2><?php echo $fl->headline; ?></h2>
                <?php endforeach; ?>
            </div>
            <div class="flexslider">
                <ul class="slides clients">
                    <?php foreach($flexslide as $fl): ?>
                        <li class="our_partners">
                            <a href="">
                                <img src="<?php echo base_url($fl->img); ?>" />
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--=== End Flex-Slider ===-->
