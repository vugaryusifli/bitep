

    <!--=== Head-Back ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="head-back">
                    <?php foreach($pages as $page): ?>
                        <?php if($page->id == 2): ?>
                            <img class="img-responsive" src="<?php echo base_url($page->image); ?>" />
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Head-Back ===-->
