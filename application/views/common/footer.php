
    <!--=== Footer ===-->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <!-- About -->
                    <div class="headline">
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 1): ?>
                                <h2><?php echo $foot->headline; ?></h2>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div>
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 1): ?>
                                <span class="head"><?php echo $foot->text_head; ?><span class="foot"><?php echo $foot->text_foot; ?></span></span>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Monthly Newsletter -->
                    <div class="headline">
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 2): ?>
                                <h2><?php echo $foot->headline; ?></h2>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div>
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 2): ?>
                                <span class="head"><?php echo $foot->text_head; ?><span class="foot"><?php echo $foot->text_foot; ?></span></span>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <form method="post" action="" id="subscribeForm" name="subscribe" class="form-inline">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" id="subscr_email" name="email" class="form-control" placeholder="Email" />
                            </div>
                            <div class="input-group">
                                <input class="btn info" id="subscribe" value="Subscribe" type="submit"  disabled="" />
                            </div>
                        </div>
                    </form>
                    <br>
                </div>
                <div class="col-md-4">
                    <!-- Monthly Newsletter -->
                    <div class="headline">
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 3): ?>
                                <h2><?php echo $foot->headline; ?></h2>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <address>
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 3): ?>
                                <span>
                                    <?php echo $foot->text_foot; ?><a href=""><?php echo $foot->email; ?></a>
                                </span>
                                <!--LiveInternet counter-->
                                <p>
                                    <a href="">
                                        <img class="img-responsive" src="<?php echo base_url($foot->gif); ?>" />
                                    </a>
                                </p>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </address>
                    <!-- Stay Connected -->
                    <div class="headline">
                        <?php foreach($footer as $foot): ?>
                            <?php if($foot->id == 4): ?>
                                <h2><?php echo $foot->headline; ?></h2>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <ul class="social-icons">
                        <?php foreach($footer as $foot): ?>
                            <li>
                                <a href="" class="<?php echo $foot->icons; ?>"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Footer ===-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="copyright-space">
                        <?php foreach($copyright as $copr): ?>
                            <?php echo $copr->text_foot; ?>
                            <a href="#"><?php echo $copr->text_link; ?></a>
                        <?php endforeach; ?>
                    </p>
                </div>
            </div><!--/row-->
        </div><!--/container-->
    </div>

    <!--=== Back-To-Top ===-->
    <div class="back-to-top">
        <button onclick="topFunction()" id="myBtn" title="Go to top"></button>
    </div>
    <!--=== End Back-To-Top ===-->

 </body>
</html>