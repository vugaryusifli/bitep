<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap-theme.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/rs/src/css/settings.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bxslider/jquery.bxslider.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/flexslider/flexslider.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/fancybox/source/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/rs/src/js/jquery.themepunch.plugins.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/rs/src/js/jquery.themepunch.revolution.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bxslider/jquery.bxslider.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/flexslider/jquery.flexslider-min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/fancybox/source/jquery.fancybox.pack.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/migrate/jquery-migrate-1.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.form.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.json.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/back-to-top.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/r-slider.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/style.js'); ?>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body>

    <!--=== Topbar ===-->
    <div class="top">
        <div class="container">
            <ul class="loginbar pull-right">
                <li>
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    <a>Languages</a>
                    <ul class="languages">
                        <?php foreach($language as $lang): ?>
                            <li>
                                <a id="lang_<?php echo $lang->lang_id; ?>" href=""> <?php echo $lang->languages; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--=== End Topbar ===-->

    <!--=== Header ===-->
    <div class="header">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">
                        <?php foreach($pages as $img): ?>
                            <?php if($img->id==1): ?>
                                <img class="img-responsive" id="logo-header" src="<?php echo base_url($img->image); ?>" alt="<?php echo $img->title; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div id="navbar" class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php foreach($menus as $menu): ?>
                            <?php if(!isset($menu->sub)): ?>
                                <li>
                                    <a id="first" href="<?php echo base_url($menu->m_url); ?>"><?php echo $menu->m_name; ?></a>
                                </li>
                            <?php else: ?>
                                <li class="dropdown">
                                    <a id="second" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menu->m_name; ?> <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($menu->sub as $sub_menu): ?>
                                            <li>
                                                <a id="third" href="<?php echo base_url($sub_menu->m_url); ?>"><?php echo $sub_menu->m_name; ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--=== End Header ===-->