<?php $this->load->view('common/head_html.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Please Log in</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('verifylogin'); ?>">
                            <fieldset>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username" name="username" />
                                    <span class="text-danger"><?php echo form_error('username'); ?></span>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" name="password" />
                                    <span class="text-danger"><?php echo form_error('password'); ?></span>
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-lg btn-primary btn-block" type="submit" name="loginSubmit" value="Login">
                                </div>

                                <a href="<?php echo base_url(); ?>"><span class="fa fa-arrow-circle-left"></span> Back to Homepage</a>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('common/foot_html.php'); ?>