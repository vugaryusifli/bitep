

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-2">
                <?php foreach($pages as $page): ?>
                    <?php if($page->id == 1 || $page->id == 2): ?>
                        <p>
                            <span class="head"><?php echo $page->content_head; ?><span class="foot"><?php echo $page->content; ?></span></span>
                        </p>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>