
    <!--=== Contact Us ===-->
    <div class="container">
        <div class="row">
            <div class="feedback">
                <div class="col-md-9">
                    <div class="wrap2">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1237.4608983417916!2d49.83813213510455!3d40.41646403533499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e0!4m0!4m3!3m2!1d40.4166168!2d49.8393431!5e0!3m2!1sru!2s!4v1469889583253"  frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-3">
                    <?php foreach($contacts as $contact): ?>
                        <span><?php echo $contact->text; ?><a href=""><?php echo $contact->href; ?></a></span>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-8">
                    <form method="post" action="<?php echo base_url('pages/contact_us'); ?>">
                        <div class="form-group">
                            <label for="name">Name <span class="color-red">*</span></label>
                            <input class="form-control" type="text" id="name" placeholder="Name" name="name" value="<?php echo set_value('name'); ?>" />
                            <span class="text-danger"><?php echo form_error('name'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="color-red">*</span></label>
                            <input class="form-control" type="text" id="email" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>" />
                            <span class="text-danger"><?php echo form_error('email'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="message">Message <span class="color-red">*</span></label>
                            <textarea id="message" placeholder="Message" name="message" rows="8" class="form-control"><?php echo set_value('message'); ?></textarea>
                            <span class="text-danger"><?php echo form_error('message'); ?></span>
                        </div>
                        <div class="form-group">
                            <input class="btn info t" value="Send message" type="submit" />
                        </div>
                    </form>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Contact Us ===-->