
    <!--=== Portfolio ===-->
    <div class="container">
        <div class="row">
            <div class="box-portfolio">
                <?php foreach($portfolios as $portfolio): ?>
                    <div class="col-md-4">
                        <div class="view view-tenth portfolio_catalog_materials">
                            <img class="img-responsive" src="<?php echo base_url($portfolio->img); ?>" />
                                <div class="mask">
                                    <h2><?php echo $portfolio->headline; ?></h2>
                                    <p><?php echo $portfolio->content; ?></p>
                                    <a href="" class="info">More</a>
                                </div>
                            <div><?php echo $portfolio->foot; ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--=== End Portfolio ===-->