
    <!--=== Slider ===-->
    <div class="fullwidthbanner-container">
         <div class="fluid-container">
            <div class="fullwidthbanner">
                <ul>
                    <!-- THE FIRST SLIDE -->
                    <li data-transition="slideleft" data-slotamount="1" data-masterspeed="300" data-thumb="">

                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <?php foreach($carousel as $slide): ?>
                            <?php if($slide->id==1): ?>
                                <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <div class="tp-caption large_text sft"
                             data-x="10"
                             data-y="44"
                             data-speed="300"
                             data-start="800"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==1)
                                {
                                   echo $slide->description;
                                }
                                ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption large_text sfr srt"
                             data-x="88"
                             data-y="78"
                             data-speed="300"
                             data-start="1100"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==2): ?>
                                    <span><?php echo $slide->description; ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption lfl"
                             data-x="100"
                             data-y="150"
                             data-speed="800"
                             data-start="900"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==3): ?>
                                    <img width="240" src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption lfl"
                             data-x="280"
                             data-y="313"
                             data-speed="400"
                             data-start="1100"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==4): ?>
                                    <img width="160" src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption lft"
                             data-x="570"
                             data-y="130"
                             data-speed="300"
                             data-start="500"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==2): ?>
                                    <img width="350" src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </li>

                    <!-- THE SECOND SLIDE -->
                    <li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300" data-thumb="">

                        <!-- THE MAIN IMAGE IN THE SECOND SLIDE -->
                        <?php foreach($carousel as $slide): ?>
                            <?php if($slide->id==5): ?>
                                <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <div class="tp-caption medium_text sfb str"
                             data-x="0"
                             data-y="12"
                             data-speed="300"
                             data-start="800"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==3): ?>
                                    <span><?php echo $slide->description; ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption randomrotate"
                             data-x="189"
                             data-y="76"
                             data-speed="600"
                             data-start="1100"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==6): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption randomrotate"
                             data-x="0"
                             data-y="92"
                             data-speed="600"
                             data-start="1200"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==7): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption randomrotate"
                             data-x="200"
                             data-y="209"
                             data-speed="600"
                             data-start="1300"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==8): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption randomrotate"
                             data-x="589"
                             data-y="201"
                             data-speed="300"
                             data-start="1600"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==9): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption randomrotate"
                             data-x="667"
                             data-y="294"
                             data-speed="300"
                             data-start="1700"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==10): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>


                        <div class="tp-caption randomrotate"
                             data-x="767"
                             data-y="71"
                             data-speed="300"
                             data-start="1900"
                             data-easing="easeOutExpo">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==11): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>


                    </li>

                    <!-- THE THIRD SLIDE -->
                    <li data-transition="papercut" data-slotamount="15" data-masterspeed="300" data-delay="9400" data-thumb="">

                        <!-- THE MAIN IMAGE IN THE THIRD SLIDE -->
                        <?php foreach($carousel as $slide): ?>
                            <?php if($slide->id==12): ?>
                                <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <div class="tp-caption very_big_white lfl stl"
                             data-x="0"
                             data-y="343"
                             data-speed="300"
                             data-start="500"
                             data-easing="easeOutExpo" data-end="8800" data-endspeed="300" data-endeasing="easeInSine">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==4): ?>
                                    <span><?php echo $slide->description; ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption big_white lfl stl"
                             data-x="0"
                             data-y="390"
                             data-speed="300"
                             data-start="800"
                             data-easing="easeOutExpo" data-end="9100" data-endspeed="300" data-endeasing="easeInSine">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==5)
                                {
                                    echo $slide->description;
                                }
                                ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="tp-caption lft ltb"
                             data-x="600"
                             data-y="0"
                             data-speed="1200"
                             data-start="1500"
                             data-easing="easeOutExpo" data-end="5100" data-endspeed="1200" data-endeasing="easeInSine">
                            <?php foreach($carousel as $slide): ?>
                                <?php if($slide->id==13): ?>
                                    <img src="<?php echo base_url($slide->image); ?>" alt="<?php echo $slide->title; ?>">
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </li>

                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
         </div>
    </div>
    <!--=== End Slider ===-->

    <!--=== Domain ===-->
    <div class="container">
         <div class="tag-box tag-box2">
             <div class="row">
                 <div class="col-md-4 col-md-offset-4 title">
                     <p class="text-primary text-center">Check domain:</p>
                 </div>
                 <form class="form-inline" method="post" action="">
                     <div class="form-group col-md-5 col-md-offset-4">
                        <label for="domain">www.</label>
                         <input type="text" id="domain" class="form-control" name="domain" placeholder="domain" />
                         <select class="form-control">
                             <?php foreach($domain as $dom): ?>
                                 <option value="<?php echo $dom->value; ?>"><?php echo $dom->hostname; ?></option>
                             <?php endforeach; ?>
                         </select>
                        <input type="submit" class="btn info" value="Check" />
                     </div>
                 </form>
             </div>
         </div>
    </div>
    <!--=== End Domain ===-->

    <!--=== Pricing ===-->
    <div class="container">
        <div class="row">
            <?php foreach($pricing as $price): ?>
                <div class="col-md-3 col-sm-6">
                    <div class="pricing">
                        <div class="pricing-head <?php echo $price->colors; ?>">
                            <h3><?php echo $price->headline; ?><em><?php echo $price->headline; ?> <span><?php echo $price->span; ?></span></em></h3>
                        </div>
                        <div class="pricing-second-head">
                            <h4><i><?php echo $price->price_one; ?> </i><?php echo $price->headline_second; ?><i><?php echo $price->price_two; ?></i></h4>
                        </div>
                        <div class="pricing-content">
                            <ul class="list-unstyled">
                                <?php for($i=0; $i<5; $i++): ?>
                                    <li><i class="fa fa-tags"></i> <?php echo $price->p_foot; ?></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                        <div class="pricing-footer">
                            <?php if($price->id >= 1 && $price->id <=4): ?>
                                <a href="" class="btn info">Order</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <hr>
    </div>
    <!--=== End Pricing ===-->

    <!--=== Bx-Slider ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="headline">
                    <?php foreach($bxslide as $bx): ?>
                        <h2><?php echo $bx->headline; ?></h2>
                    <?php endforeach; ?>
                </div>
                <div class="bx-wrapper">
                    <ul class="bxslider recent-work">
                        <?php foreach($bxslide as $bx): ?>
                            <li>
                                <a href="">
                                    <em class="overflow-hidden">
                                        <img src="<?php echo base_url($bx->img); ?>" alt="<?php echo $bx->title; ?>" />
                                    </em>
                                    <span>
                                        <strong><?php echo $bx->text_head; ?></strong>
                                        <i><?php echo $bx->text_foot; ?></i>
                                    </span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="wrap">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FBitepOfficial%2F%3Ffref%3Dts&tabs=timeline&width=263&height=427&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Bx-Slider ===-->