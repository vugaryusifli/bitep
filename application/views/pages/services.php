
    <!-- Service Blocks -->
    <div class="container">
        <div class="row">
            <div class="box-services">
                <?php foreach($services as $service): ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="service-block-in service-block-colored <?php echo $service->color; ?>">
                            <a href="">
                                <h4><?php echo $service->headline; ?></h4>
                                <div>
                                    <i class="<?php echo $service->icons; ?>"></i>
                                </div>
                                <p><?php echo $service->foot; ?></p>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- End Service Blocks -->
