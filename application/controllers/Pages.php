<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller{

    public function __construct(){
        parent:: __construct();
        $this->load->model('Pages_model');

        if($this->input->post()){
            $obj = array(
                'email' => $this->input->post('email')
            );
            $this->db->insert('tbl_subscribe', $obj);
        }

    }

    public function index(){
        $data['title'] = 'Main';
        $data['carousel'] = $this->Pages_model->get_slide();
        $data['pages'] = $this->Pages_model->get_page();
        $data['pricing'] = $this->Pages_model->get_pricing();
        $data['language'] = $this->Pages_model->get_language();
        $data['domain'] = $this->Pages_model->get_domain();
        $data['bxslide'] = $this->Pages_model->get_bx_slide();
        $data['flexslide'] = $this->Pages_model->get_flex_slide();
        $data['footer'] = $this->Pages_model->get_footer();
        $data['copyright'] = $this->Pages_model->get_copyright();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('pages/main', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function about_us(){
        $data['title']= 'About Us';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('common/about_img', $data);
        $this->load->view('pages/about_us', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function services(){
        $data['title']= 'Services';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $data['services']=$this->Pages_model->get_services();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('pages/services', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function portfolio(){
        $data['title']= 'Portfolio';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $data['portfolios']=$this->Pages_model->get_portfolio();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('pages/portfolio', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function contact_us(){
        $data['title']= 'Contact Us';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $data['contacts']=$this->Pages_model->get_contact();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {

        }
        else
        {
            $name = $this->input->post('name');
            $from_email = $this->input->post('email');
            $message = $this->input->post('message');

            $to_email = 'vuqar.yusifli.94@gmail.com';


            $this->email->from($from_email, $name);
            $this->email->to($to_email);
            $this->email->message($message);
            if ($this->email->send())
            {
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
                redirect('pages/contact_us');
            }
            else
            {
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is error in sending mail! Please try again later</div>');
                redirect('pages/contact_us');
            }

        }

        $this->load->view('common/header', $data);
        $this->load->view('pages/contact_us', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);

    }


    public function blog(){
        $data['title']= 'Blog';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('pages/blog', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function manifest(){
        $data['title']= 'Manifest';
        $data['pages']=$this->Pages_model->get_page();
        $data['language']=$this->Pages_model->get_language();
        $data['footer']=$this->Pages_model->get_footer();
        $data['copyright']=$this->Pages_model->get_copyright();
        $data['flexslide']=$this->Pages_model->get_flex_slide();
        $query = $this->Pages_model->get_menu();
        foreach ($query as $menu) {
            if ($menu->parent_id == 0) {
                $data['menus'][$menu->m_id] = $menu;
            } else {
                $data['menus'][$menu->parent_id]->sub[] = $menu;
            }
        }
        $this->load->view('common/header', $data);
        $this->load->view('common/about_img', $data);
        $this->load->view('pages/manifest', $data);
        $this->load->view('common/flex_slide', $data);
        $this->load->view('common/footer', $data);
    }

    public function form_validation(){

        $this->form_validation->set_rules('email', 'Email', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '');
            redirect('pages');
        }
        else
        {
            $this->session->set_flashdata('msg', '');
            redirect('pages');
        }
    }



}