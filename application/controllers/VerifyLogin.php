<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifyLogin extends CI_Controller{

    public function __construct(){
        parent:: __construct();
        $this->load->model('User');
    }

    public function index(){
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            //Field validation failed.  User redirected to login page
            $this->load->view('admin/login_view');
        }
        else
        {
            //Go to private area
            redirect('home', 'refresh');
        }
    }

    public function check_database($password){
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');

        //query the database
        $result = $this->User->login($username, $password);

        if($result){
            $sess_array = array();
            foreach($result as $row){
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return true;
        }else{
            $this->form_validation->set_message('check_database', 'Invalid Username or Password');
            return false;
        }
    }

}
