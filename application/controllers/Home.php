<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Pages_model');
    }

    public function index(){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->view('home_view', $data);
        }else{
            //If no session, redirect to login page
            redirect('admin', 'refresh');
        }
    }

    public function logout(){
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }

    public function updatePage(){
        $this->load->view('common/head.php');
        $this->load->view('simple_page/updatePage');
        $this->load->view('common/foot.php');
    }


}